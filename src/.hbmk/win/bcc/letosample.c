/*
 * Harbour 3.2.0dev (r1607181832)
 * Borland C++ 5.5.1 (32-bit)
 * Generated C source from "letosample.prg"
 */

#include "hbvmpub.h"
#include "hbinit.h"


HB_FUNC( MAIN );
HB_FUNC_EXTERN( RDDSETDEFAULT );
HB_FUNC_EXTERN( LETO_CONNECT );
HB_FUNC_EXTERN( MSG );
HB_FUNC_EXTERN( DBCREATE );
HB_FUNC_EXTERN( DBUSEAREA );
HB_FUNC_EXTERN( ORDCONDSET );
HB_FUNC_EXTERN( RECNO );
HB_FUNC_EXTERN( ORDCREATE );
HB_FUNC_EXTERN( RLOCK );
HB_FUNC_EXTERN( DBAPPEND );
HB_FUNC_EXTERN( STR );
HB_FUNC_EXTERN( DATE );
HB_FUNC_EXTERN( DBUNLOCK );
HB_FUNC_EXTERN( DBSEEK );
HB_FUNC_EXTERN( DBCLOSEAREA );
HB_FUNC_EXTERN( LETO );
HB_FUNC_EXTERN( __DBGENTRY );
HB_FUNC_INITLINES();


HB_INIT_SYMBOLS_BEGIN( hb_vm_SymbolInit_LETOSAMPLE )
{ "MAIN", {HB_FS_PUBLIC | HB_FS_FIRST | HB_FS_LOCAL}, {HB_FUNCNAME( MAIN )}, NULL },
{ "RDDSETDEFAULT", {HB_FS_PUBLIC}, {HB_FUNCNAME( RDDSETDEFAULT )}, NULL },
{ "LETO_CONNECT", {HB_FS_PUBLIC}, {HB_FUNCNAME( LETO_CONNECT )}, NULL },
{ "MSG", {HB_FS_PUBLIC}, {HB_FUNCNAME( MSG )}, NULL },
{ "DBCREATE", {HB_FS_PUBLIC}, {HB_FUNCNAME( DBCREATE )}, NULL },
{ "DBUSEAREA", {HB_FS_PUBLIC}, {HB_FUNCNAME( DBUSEAREA )}, NULL },
{ "ORDCONDSET", {HB_FS_PUBLIC}, {HB_FUNCNAME( ORDCONDSET )}, NULL },
{ "RECNO", {HB_FS_PUBLIC}, {HB_FUNCNAME( RECNO )}, NULL },
{ "ORDCREATE", {HB_FS_PUBLIC}, {HB_FUNCNAME( ORDCREATE )}, NULL },
{ "CODI", {HB_FS_PUBLIC | HB_FS_MEMVAR}, {NULL}, NULL },
{ "I", {HB_FS_PUBLIC | HB_FS_MEMVAR}, {NULL}, NULL },
{ "TESTE", {HB_FS_PUBLIC}, {NULL}, NULL },
{ "RLOCK", {HB_FS_PUBLIC}, {HB_FUNCNAME( RLOCK )}, NULL },
{ "DBAPPEND", {HB_FS_PUBLIC}, {HB_FUNCNAME( DBAPPEND )}, NULL },
{ "STR", {HB_FS_PUBLIC}, {HB_FUNCNAME( STR )}, NULL },
{ "NOME", {HB_FS_PUBLIC | HB_FS_MEMVAR}, {NULL}, NULL },
{ "DATE", {HB_FS_PUBLIC}, {HB_FUNCNAME( DATE )}, NULL },
{ "DATA", {HB_FS_PUBLIC | HB_FS_MEMVAR}, {NULL}, NULL },
{ "DBUNLOCK", {HB_FS_PUBLIC}, {HB_FUNCNAME( DBUNLOCK )}, NULL },
{ "DBSEEK", {HB_FS_PUBLIC}, {HB_FUNCNAME( DBSEEK )}, NULL },
{ "DBCLOSEAREA", {HB_FS_PUBLIC}, {HB_FUNCNAME( DBCLOSEAREA )}, NULL },
{ "LETO", {HB_FS_PUBLIC}, {HB_FUNCNAME( LETO )}, NULL },
{ "__DBGENTRY", {HB_FS_PUBLIC}, {HB_FUNCNAME( __DBGENTRY )}, NULL },
{ "(_INITLINES)", {HB_FS_INITEXIT | HB_FS_LOCAL}, {hb_INITLINES}, NULL }
HB_INIT_SYMBOLS_EX_END( hb_vm_SymbolInit_LETOSAMPLE, "letosample.prg", 0x0, 0x0003 )

#if defined( HB_PRAGMA_STARTUP )
   #pragma startup hb_vm_SymbolInit_LETOSAMPLE
#elif defined( HB_DATASEG_STARTUP )
   #define HB_DATASEG_BODY    HB_DATASEG_FUNC( hb_vm_SymbolInit_LETOSAMPLE )
   #include "hbiniseg.h"
#endif

HB_FUNC( MAIN )
{
	static const HB_BYTE pcode[] =
	{
		13,1,0,51,108,101,116,111,115,97,109,112,108,101,
		46,112,114,103,58,77,65,73,78,0,36,4,0,37,
		1,0,67,83,69,82,86,69,82,0,106,18,47,47,
		49,50,55,46,48,46,48,46,49,58,50,56,49,50,
		47,0,80,1,36,6,0,176,1,0,106,5,76,69,
		84,79,0,20,1,36,7,0,176,2,0,95,1,12,
		1,92,255,8,28,49,36,8,0,176,3,0,106,28,
		83,101,109,32,99,111,110,101,120,97,111,32,99,111,
		109,32,111,32,115,101,114,118,105,100,111,114,32,0,
		95,1,72,20,1,36,9,0,100,110,7,36,11,0,
		176,3,0,106,23,83,101,114,118,105,100,111,114,32,
		101,115,99,117,116,97,110,100,111,32,101,109,58,0,
		95,1,72,20,1,36,19,0,176,4,0,95,1,106,
		6,84,69,83,84,69,0,72,106,5,67,79,68,73,
		0,106,2,78,0,92,3,121,4,4,0,106,5,78,
		79,77,69,0,106,2,67,0,92,40,121,4,4,0,
		106,5,67,73,68,65,0,106,2,67,0,92,30,121,
		4,4,0,106,5,69,83,84,65,0,106,2,67,0,
		92,2,121,4,4,0,106,5,68,65,84,65,0,106,
		2,68,0,92,8,121,4,4,0,4,5,0,20,2,
		36,21,0,176,5,0,120,100,95,1,106,6,116,101,
		115,116,101,0,72,106,6,116,101,115,116,101,0,20,
		4,36,23,0,176,6,0,100,100,100,100,100,100,176,
		7,0,12,0,100,100,100,100,100,100,100,100,100,100,
		100,100,100,20,20,176,8,0,100,106,5,67,111,100,
		105,0,106,5,67,111,100,105,0,90,27,51,108,101,
		116,111,115,97,109,112,108,101,46,112,114,103,58,77,
		65,73,78,0,109,9,0,6,100,20,5,36,25,0,
		122,165,83,10,0,25,104,36,26,0,85,108,11,74,
		176,12,0,20,0,74,36,27,0,85,108,11,74,176,
		13,0,20,0,74,36,28,0,109,10,0,108,11,76,
		9,36,29,0,106,10,82,101,103,105,115,116,114,111,
		32,0,176,14,0,109,10,0,92,3,12,2,72,108,
		11,76,15,36,30,0,176,16,0,12,0,108,11,76,
		17,36,31,0,85,108,11,74,176,18,0,20,0,74,
		36,25,0,109,10,0,23,21,83,10,0,92,100,15,
		28,151,36,33,0,85,108,11,74,176,19,0,92,50,
		20,1,74,36,34,0,176,3,0,108,11,87,15,20,
		1,36,35,0,85,108,11,74,176,20,0,20,0,74,
		36,36,0,100,110,7
	};

	hb_vmExecute( pcode, symbols );
}

HB_FUNC_INITLINES()
{
	static const HB_BYTE pcode[] =
	{
		51,108,101,116,111,115,97,109,112,108,101,46,112,114,
		103,58,40,95,73,78,73,84,76,73,78,69,83,41,
		0,106,15,108,101,116,111,115,97,109,112,108,101,46,
		112,114,103,0,121,106,6,208,11,168,254,30,0,4,
		3,0,4,1,0,110,7
	};

	hb_vmExecute( pcode, symbols );
}

#line 38 "letosample.prg"
#include "windows.h"
#include "hbapi.h"

HB_FUNC(  MSG )
{
   MessageBox( GetActiveWindow(), hb_parc(1), "Ok", 0 );
}
