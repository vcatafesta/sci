/* C source generated by Harbour */

#include "hbvmpub.h"
#include "hbinit.h"

HB_FUNC( HIPERSEEK );
HB_FUNC_EXTERN( SCROLL );
HB_FUNC_EXTERN( SETPOS );
HB_FUNC_EXTERN( DBUSEAREA );
HB_FUNC_EXTERN( FILE );
HB_FUNC_EXTERN( QOUT );
HB_FUNC_EXTERN( HS_INDEX );
HB_FUNC_EXTERN( HS_OPEN );
HB_FUNC_EXTERN( INKEY );
HB_FUNC_EXTERN( HS_SET );
HB_FUNC_EXTERN( HS_NEXT );
HB_FUNC_EXTERN( DBGOTO );
HB_FUNC_EXTERN( HS_VERIFY );
HB_FUNC_EXTERN( HS_CLOSE );

HB_INIT_SYMBOLS_BEGIN( hb_vm_SymbolInit_HIPERSEEK )
{ "HIPERSEEK", { HB_FS_PUBLIC | HB_FS_FIRST | HB_FS_LOCAL }, { HB_FUNCNAME( HIPERSEEK ) }, NULL },
{ "SCROLL", { HB_FS_PUBLIC }, { HB_FUNCNAME( SCROLL ) }, NULL },
{ "SETPOS", { HB_FS_PUBLIC }, { HB_FUNCNAME( SETPOS ) }, NULL },
{ "DBUSEAREA", { HB_FS_PUBLIC }, { HB_FUNCNAME( DBUSEAREA ) }, NULL },
{ "FILE", { HB_FS_PUBLIC }, { HB_FUNCNAME( FILE ) }, NULL },
{ "QOUT", { HB_FS_PUBLIC }, { HB_FUNCNAME( QOUT ) }, NULL },
{ "HS_INDEX", { HB_FS_PUBLIC }, { HB_FUNCNAME( HS_INDEX ) }, NULL },
{ "HS_OPEN", { HB_FS_PUBLIC }, { HB_FUNCNAME( HS_OPEN ) }, NULL },
{ "INKEY", { HB_FS_PUBLIC }, { HB_FUNCNAME( INKEY ) }, NULL },
{ "HS_SET", { HB_FS_PUBLIC }, { HB_FUNCNAME( HS_SET ) }, NULL },
{ "HS_NEXT", { HB_FS_PUBLIC }, { HB_FUNCNAME( HS_NEXT ) }, NULL },
{ "DBGOTO", { HB_FS_PUBLIC }, { HB_FUNCNAME( DBGOTO ) }, NULL },
{ "HS_VERIFY", { HB_FS_PUBLIC }, { HB_FUNCNAME( HS_VERIFY ) }, NULL },
{ "HS_CLOSE", { HB_FS_PUBLIC }, { HB_FUNCNAME( HS_CLOSE ) }, NULL }
HB_INIT_SYMBOLS_EX_END( hb_vm_SymbolInit_HIPERSEEK, "", 0x0, 0x0003 )

#if defined( HB_PRAGMA_STARTUP )
   #pragma startup hb_vm_SymbolInit_HIPERSEEK
#elif defined( HB_DATASEG_STARTUP )
   #define HB_DATASEG_BODY    HB_DATASEG_FUNC( hb_vm_SymbolInit_HIPERSEEK )
   #include "hbiniseg.h"
#endif

HB_FUNC( HIPERSEEK )
{
	static const HB_BYTE pcode[] =
	{
		13,5,0,36,5,0,106,62,82,101,99,101,98,101,114,45,62,67,111,
		100,105,32,43,32,82,101,99,101,98,101,114,45,62,78,111,109,101,32,
		43,32,82,101,99,101,98,101,114,45,62,67,105,100,97,32,43,32,82,
		101,99,101,98,101,114,45,62,69,110,100,101,0,80,1,36,6,0,106,
		4,123,124,124,0,95,1,72,106,2,125,0,72,40,11,80,2,36,7,
		0,106,4,82,85,65,0,80,3,121,80,4,121,80,5,36,9,0,176,
		1,0,20,0,176,2,0,121,121,20,2,36,10,0,176,3,0,9,100,
		106,8,114,101,99,101,98,101,114,0,100,9,9,20,6,36,12,0,176,
		4,0,106,12,82,69,67,69,66,69,82,46,72,83,88,0,12,1,31,
		71,36,13,0,176,5,0,106,29,66,117,105,108,100,105,110,103,32,72,
		105,80,101,114,45,83,69,69,75,32,73,110,100,101,120,46,46,46,0,
		20,1,36,14,0,176,6,0,106,12,82,69,67,69,66,69,82,46,72,
		83,88,0,95,1,92,2,12,3,80,4,25,29,36,16,0,176,7,0,
		106,12,82,69,67,69,66,69,82,46,72,83,88,0,92,8,122,12,3,
		80,4,36,18,0,176,8,0,121,20,1,36,20,0,176,9,0,95,4,
		95,3,20,2,36,21,0,176,10,0,95,4,12,1,80,5,36,23,0,
		95,5,121,15,28,50,36,24,0,176,11,0,95,5,20,1,36,25,0,
		176,12,0,95,2,95,3,12,2,28,12,36,26,0,176,5,0,95,5,
		20,1,36,28,0,176,10,0,95,4,12,1,80,5,25,201,36,31,0,
		176,13,0,95,4,20,1,7
	};

	hb_vmExecute( pcode, symbols );
}
